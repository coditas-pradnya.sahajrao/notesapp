import { Component, OnInit } from '@angular/core';
import { NotesService } from 'src/app/shared/notes.service';
import {Note}from '../../../shared/note.model';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnInit {
  notes:Note[]=new Array<Note>();
  constructor(private notesService : NotesService) { }

  ngOnInit(){
   this.notes = this.notesService.getAll();
  }

}
