import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './Component/Pages/main-layout/main-layout.component';
import { NoteDetailsComponent } from './Component/Pages/note-details/note-details.component';
import { NoteListComponent } from './Component/Pages/note-list/note-list.component';

const routes: Routes = [
  {path:"", component:MainLayoutComponent , children:[
    {path:"", component:NoteListComponent},
    {path:":id", component:NoteDetailsComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
