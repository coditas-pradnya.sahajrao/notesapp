import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoteListComponent } from './Component/Pages/note-list/note-list.component';
import { MainLayoutComponent } from './Component/Pages/main-layout/main-layout.component';
import { NoteCardComponent } from './Component/Pages/note-card/note-card.component';
import { NoteDetailsComponent } from './Component/Pages/note-details/note-details.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NoteListComponent,
    MainLayoutComponent,
    NoteCardComponent,
    NoteDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
